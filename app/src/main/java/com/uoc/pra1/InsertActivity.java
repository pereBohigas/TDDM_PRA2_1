package com.uoc.pra1;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.uoc.datalevel.DataException;
import com.uoc.datalevel.DataObject;
import com.uoc.datalevel.SaveCallback;

public class InsertActivity extends AppCompatActivity {

    ImageView view;
    Bitmap image;
    EditText text;
    String name;
    String price;
    String description;
    DataObject new_item;
    Uri path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR2 :: Insert");



        final Button button1 = (Button) findViewById(R.id.selectImage);
        button1.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                loadImage();
            }
        });

        final Button button2 = (Button) findViewById(R.id.insertItem);
        button2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                view = (ImageView) findViewById(R.id.imageProduct);
                image = ((BitmapDrawable) view.getDrawable()).getBitmap();
                text = (EditText) findViewById(R.id.nameValue);
                name = text.getText().toString();
                text = (EditText) findViewById(R.id.priceValue);
                price = text.getText().toString();
                text = (EditText) findViewById(R.id.descriptionValue);
                description = text.getText().toString();
                new_item = new DataObject(name);
                new_item.saveInBackground(new SaveCallback<DataObject>()
                {
                    @Override
                    public void done (DataObject object, DataException e){
                        if (e == null) {
                            new_item.put("name",name);
                            new_item.put("price",price);
                            new_item.put("description",description);
                            new_item.put("image", image);
                        } else {

                        }
                    }
                });
            }
        });

    }

    private void loadImage() {
        Intent intent = new Intent (Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/");
        startActivityForResult(intent.createChooser(intent,"Select the image"),10);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==RESULT_OK){
            path = data.getData();
            view = ((ImageView) findViewById(R.id.imageProduct));
            view.setImageURI(path);
            view.refreshDrawableState();
        }
    }

    @Override
    public boolean onTouchEvent (MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService (Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        return true;
    }

}
